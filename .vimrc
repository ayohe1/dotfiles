" -----------------------------------------------
"           _
"   _   __ (_)____ ___   _____ _____
"  | | / // // __ `__ \ / ___// ___/
"  | |/ // // / / / / // /   / /__
"  |___//_//_/ /_/ /_//_/    \___/
"
" -----------------------------------------------
" -----------------------------------------------
" basic settings {{{1

" -----------------------------------------------
" NeoBundle {{{2
" Note: Skip initialization for vim-tiny or vim-small.
if 0 | endif

if has('vim_starting')
  if &compatible
    set nocompatible               " Be iMproved
  endif

  " Required:
  set runtimepath+=~/.vim/bundle/neobundle.vim/
endif

" Required:
call neobundle#begin(expand('~/.vim/bundle/'))

" Let NeoBundle manage NeoBundle
" Required:
NeoBundleFetch 'Shougo/neobundle.vim'

" My Bundles here:
" Refer to |:NeoBundle-examples|.
" Note: You don't set neobundle setting in .gvimrc!

NeoBundle 'vim-jp/vimdoc-ja' " Document transrate ja
NeoBundle 'vim-jp/autofmt'
NeoBundle 'vim-jp/vim-go-extra'

NeoBundle 'thinca/vim-quickrun'
NeoBundle 'scrooloose/nerdtree'
NeoBundle 'scrooloose/syntastic'
NeoBundle 'mattn/sonictemplate-vim'
NeoBundle 'fatih/vim-go'
NeoBundle 'vim-scripts/tagbar'
NeoBundle 'szw/vim-tags'
NeoBundle 'powerline/powerline'
NeoBundle 'bling/vim-airline'

NeoBundle 'Shougo/unite.vim'
NeoBundle 'Shougo/unite-outline'
NeoBundle 'Shougo/neocomplete'
NeoBundle 'Shougo/neosnippet'
NeoBundle 'Shougo/neosnippet-snippets'

NeoBundle 'tpope/vim-markdown'
NeoBundle 'tpope/vim-unimpaired'
NeoBundle 'tpope/vim-commentary'
NeoBundle 'tpope/vim-surround'

" Markdown plugin
NeoBundle 'plasticboy/vim-markdown'
NeoBundle 'kannokanno/previm'
NeoBundle 'tyru/open-browser.vim'

NeoBundle 'fuenor/im_control.vim'

" ColorScheme
" solarized カラースキーム
  NeoBundle 'altercation/vim-colors-solarized'
" mustang カラースキーム
  NeoBundle 'croaker/mustang-vim'
" wombat カラースキーム
  NeoBundle 'jeffreyiacono/vim-colors-wombat'
" jellybeans カラースキーム
  NeoBundle 'nanotech/jellybeans.vim'
" lucius カラースキーム
  NeoBundle 'vim-scripts/Lucius'
" zenburn カラースキーム
  NeoBundle 'vim-scripts/Zenburn'
" mrkn256 カラースキーム
  NeoBundle 'mrkn/mrkn256.vim'
" railscasts カラースキーム
  NeoBundle 'jpo/vim-railscasts-theme'
" pyte カラースキーム
  NeoBundle 'therubymug/vim-pyte'
" molokai カラースキーム
  NeoBundle 'tomasr/molokai'

" カラースキーム一覧表示に Unite.vim を使う
  NeoBundle 'Shougo/unite.vim'
  NeoBundle 'ujihisa/unite-colorscheme'

call neobundle#end()

" Required:
filetype plugin indent on

" If there are uninstalled bundles found on startup,
" this will conveniently prompt you to install them.
NeoBundleCheck
"}}}

" -----------------------------------------------
" setting options {{{2
" encoding
set encoding=utf8 ""default encoding
set termencoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8,euc-jp,sjis,iso-2022-jp,cp932

" windows gvim menu utf-8
source $VIMRUNTIME/delmenu.vim
set langmenu-=ja_jp.utf8
source $VIMRUNTIME/menu.vim

set clipboard=unnamed,autoselect
set completeopt=menu,preview
set nobackup
set autoindent
set shiftround
set noexpandtab
set wrap "
set wrapscan "
set whichwrap+=h
set whichwrap+=l
set showmatch
set ruler " show ruler
set laststatus=2
set scrolloff=5

set smarttab
set cindent

set vb t_vb=
" set imdisable "don't use im
" set noimcmdline " no im at cmd
set iminsert=0
set imsearch=0

set hlsearch " serch result highlight
set incsearch " incliment serch
set backspace=indent,eol,start "backspace delete everything
set hidden

set formatoptions-=r " don't comment when CR
set formatoptions-=o

set fileformats=unix,dos,mac
set textwidth=0

set autoread " auto read when file change
set history=100

set helplang=ja,en
set foldmethod=marker
set nobackup
set number
set noundofile
syntax on

" set rulerformat=%45(%12f%=\ %m%{'['.(&fenc!=''?&fenc:&enc).']'}\ %l-%v\ %p%%\ [%02B]%)
" set statusline=%f:\ %{substitute(getcwd(),'.*/','','')}\ %m%=%{(&fenc!=''?&fenc:&enc).':'.strpart(&ff,0,1)}\ %l-%v\ %p%%\ %02B

if exists('+breakindent')
    set breakindent
    set breakindentopt=shift:-4
    let &showbreak = '>>> '
endif

"}}}

" -----------------------------------------------
" key map {{{2
let mapleader = ","

"}}}
" -----------------------------------------------
" file type {{{2
filetype on
filetype indent on
filetype plugin on

" for Golang
filetype plugin indent on
syntax on
set path+=$GOPATH/src/**
let g:godef_split = 3
let g:syntastic_mode_map = { 'mode': 'passive',
			\ 'active_filetypes': ['go'] }
let g:syntastic_go_checkers = ['go', 'golint']
au FileType go nmap <leader>r <Plug>(go-run)
au FileType go nmap <leader>b <Plug>(go-build)
au FileType go nmap <leader>t <Plug>(go-test)
au FileType go nmap <leader>c <Plug>(go-coverage)
au FileType go nmap <Leader>ds <Plug>(go-def-split)
au FileType go nmap <Leader>dv <Plug>(go-def-vertical)
au FileType go nmap <Leader>dt <Plug>(go-def-tab)
au FileType go nmap <Leader>gd <Plug>(go-doc)
au FileType go nmap <Leader>gv <Plug>(go-doc-vertical)
au FileType go nmap <Leader>s <Plug>(go-implements)
au FileType go nmap <Leader>e <Plug>(go-rename)
let g:go_fmt_command = "goimports"
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_structs = 1
let g:go_highlight_operators = 1
let g:go_highlight_build_constraints = 1
au BufNewFile,Bufread *.go set tabstop=4 shiftwidth=4 softtabstop=4

" for markdonw
au Bufread,BufNewFile *.md set filetype=markdown


"}}}
" -----------------------------------------------
" command {{{2

"}}}
"}}}

" -----------------------------------------------
" plugins {{{1

" -----------------------------------------------
" for color scheme {{{2
let g:molokai_original = 1
let g:rehash256 = 1
colorscheme molokai
"}}}

" -----------------------------------------------
" for tagbar {{{2
nmap <F8> :TagbarToggle<CR>
"}}}


" -----------------------------------------------
" for vim-quickrun {{{2
 let g:quickrun_config = {'*': {'hook/time/enable': '1'},}
 let g:quickrun_config.go= { 'config': 'go run' }
"}}}

" -----------------------------------------------
" for nerdtree {{{2
map <silent> <leader>n :NERDTreeToggle<cr>
"}}}

" -----------------------------------------------
" for syntastic {{{2
let g:syntastic_enable_signs=1
let g:syntastic_auto_loc_list=1
"}}}
" -----------------------------------------------
" for neocomplete {{{2
"Note: This option must set it in .vimrc(_vimrc).  NOT IN .gvimrc(_gvimrc)!
" Disable AutoComplPop.
let g:acp_enableAtStartup = 0
" Use neocomplete.
let g:neocomplete#enable_at_startup = 1
" Use smartcase.
let g:neocomplete#enable_smart_case = 1
" Set minimum syntax keyword length.
let g:neocomplete#sources#syntax#min_keyword_length = 3
let g:neocomplete#lock_buffer_name_pattern = '\*ku\*'

" Define dictionary.
let g:neocomplete#sources#dictionary#dictionaries = {
    \ 'default' : '',
    \ 'vimshell' : $HOME.'/.vimshell_hist',
    \ 'scheme' : $HOME.'/.gosh_completions'
        \ }

" Define keyword.
if !exists('g:neocomplete#keyword_patterns')
    let g:neocomplete#keyword_patterns = {}
endif
let g:neocomplete#keyword_patterns['default'] = '\h\w*'

" Plugin key-mappings.
inoremap <expr><C-g>     neocomplete#undo_completion()
inoremap <expr><C-l>     neocomplete#complete_common_string()

" Recommended key-mappings.
" <CR>: close popup and save indent.
inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>
function! s:my_cr_function()
  return neocomplete#close_popup() . "\<CR>"
  " For no inserting <CR> key.
  "return pumvisible() ? neocomplete#close_popup() : "\<CR>"
endfunction
" <TAB>: completion.
inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
" <C-h>, <BS>: close popup and delete backword char.
inoremap <expr><C-h> neocomplete#smart_close_popup()."\<C-h>"
inoremap <expr><BS> neocomplete#smart_close_popup()."\<C-h>"
inoremap <expr><C-y>  neocomplete#close_popup()
inoremap <expr><C-e>  neocomplete#cancel_popup()
" Close popup by <Space>.
"inoremap <expr><Space> pumvisible() ? neocomplete#close_popup() : "\<Space>"

" For cursor moving in insert mode(Not recommended)
"inoremap <expr><Left>  neocomplete#close_popup() . "\<Left>"
"inoremap <expr><Right> neocomplete#close_popup() . "\<Right>"
"inoremap <expr><Up>    neocomplete#close_popup() . "\<Up>"
"inoremap <expr><Down>  neocomplete#close_popup() . "\<Down>"

" Enable omni completion.
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags

" Enable heavy omni completion.
if !exists('g:neocomplete#sources#omni#input_patterns')
  let g:neocomplete#sources#omni#input_patterns = {}
endif
let g:neocomplete#sources#omni#input_patterns.php = '[^. \t]->\h\w*\|\h\w*::'
let g:neocomplete#sources#omni#input_patterns.c = '[^.[:digit:] *\t]\%(\.\|->\)'
let g:neocomplete#sources#omni#input_patterns.cpp = '[^.[:digit:] *\t]\%(\.\|->\)\|\h\w*::'

" For perlomni.vim setting.
" https://github.com/c9s/perlomni.vim
let g:neocomplete#sources#omni#input_patterns.perl = '\h\w*->\h\w*\|\h\w*::'
"}}}

" -----------------------------------------------
" for neosnippet {{{2
" Plugin key-mappings.
imap <C-k>     <Plug>(neosnippet_expand_or_jump)
smap <C-k>     <Plug>(neosnippet_expand_or_jump)
xmap <C-k>     <Plug>(neosnippet_expand_target)

" SuperTab like snippets behavior.
" imap <expr><TAB> neosnippet#expandable_or_jumpable() ?
" \ "\<Plug>(neosnippet_expand_or_jump)"
" \: pumvisible() ? "\<C-n>" : "\<TAB>"
" smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
" \ "\<Plug>(neosnippet_expand_or_jump)"
" \: "\<TAB>"

" For snippet_complete marker.
if has('conceal')
" set conceallevel=2 concealcursor=i
endif
"}}}

" -----------------------------------------------
" for unite {{{2

"}}}

" -----------------------------------------------
" for vim-tags {{{2
" au BufNewFile,BufRead *.go let g:vim_tags_project_tags_command = 
" 	\ctags --languages=golang -f ~/golang.tags `pwd` 2>/dev/null &
" nnoremap <C-]> g<C-]>
"}}}

" -----------------------------------------------
" for go-tags {{{2
let g:tagbar_type_go = {
    \ 'ctagstype' : 'go',
    \ 'kinds'     : [
        \ 'p:package',
        \ 'i:imports:1',
        \ 'c:constants',
        \ 'v:variables',
        \ 't:types',
        \ 'n:interfaces',
        \ 'w:fields',
        \ 'e:embedded',
        \ 'm:methods',
        \ 'r:constructor',
        \ 'f:functions'
    \ ],
    \ 'sro' : '.',
    \ 'kind2scope' : {
        \ 't' : 'ctype',
        \ 'n' : 'ntype'
    \ },
    \ 'scope2kind' : {
        \ 'ctype' : 't',
        \ 'ntype' : 'n'
    \ },
    \ 'ctagsbin'  : 'gotags',
    \ 'ctagsargs' : '-sort -silent'
    \ }
"}}}

"}}}
